"""
Utility to get the factory details from Bangaladesh accord organization
"""
import json
import requests
import sys
import time


def make_request():
    """
    Function to make a request to the destination site & fetch the JSON data
    The request contains following parameters,
    status - Status of the factories, separated by comma
    designation - designated status
    language - language generally english
    limit - record limit
    :return json_data: JSON response
    """
    url = 'https://accord2.fairfactories.org/api/v1/factories'
    params = dict(
        status='active,inactive,no brand,pending closure',
        designation='behindschedule,ontrack,completed,initialcompleted',
        language='en',
        limit=1562
    )
    try:
        response = requests.get(url=url, params=params)
        json_data = response.json()
    except requests.exceptions.RequestException as request_exception:
        print(request_exception)
        sys.exit(1)

    return json_data


def make_result_json_object(result):
    """
    Function to prepare the result JSON object
    :param result:
    :return json_obj:
    """
    factory_name = result['name']
    designation_status = result['designation']['status']
    workers = result['workers']
    workers_int = 0
    # Check whether the value is inter type or not
    try:
        workers_int = int(workers)
    except ValueError:
        pass
    progress = int(result['progress'] * 100)

    json_obj = {
        'factoryName': factory_name,
        'remidationStatus': designation_status,
        'workerCount': workers_int,
        'progressRate': progress
    }

    return json_obj


def parse_json_data(json_data):
    """
    Function to parse the JSON factory data
    :param json_data:
    :return json_obj_list:
    """
    json_obj_list = []
    if 'results' not in json_data:
        raise ValueError("No results in given data")
    for result in json_data['results']:
        json_obj = make_result_json_object(result)
        json_obj_list.append(json_obj)

    return json_obj_list


def serialize_data(parse_result):
    """
    Function to persist the data into file
    :param parse_result:
    :return: None
    """
    with open('result_data.json', 'w') as outfile:
        json.dump(parse_result, outfile)


def execute_parser():
    """
    Function to execute the parsing process to get the factories from it
    :return: None
    """
    json_response = make_request()
    parse_result = parse_json_data(json_response)
    print(json.dumps(parse_result))
    serialize_data(parse_result)


if __name__ == '__main__':
    start_time = time.time()
    execute_parser()
    print("--- Executed in %s seconds ---" % (time.time() - start_time))
