# Accord Challenge

Bangladesh Accord is a a non profit NGO organization that monitors and keeps records of labor and sustainable ratings of most major bangladeshi suppliers. 


There are currently 1564 Factories on the list of accord related factories. 

https://bangladeshaccord.org/factories

Create an algorithm in any of the following languages **Python, Node JS, Ruby, Go** to parse this website and return information on the factories in the following format. 


```
    {
        "factoryName: "abc...",
        "remidationStatus": "",
        "workerCount" : 33 // number,
        "progressRate" : 100 // number
    }
```

Each factory should be a json object.  The idea is to  run a command that will generate a json file containing the array of JSON objects.

Please feel free to ask any questions to 

**gebin@sundar.io**

# Amfori Challenge

Amfori is a a non profit NGO organization that monitors and keeps records of labor and sustainable ratings of most major suppliers accros the world.

Write an alogirthm to get all products from all pages in one single command.

https://www.amfori.org/members

The json dump should be in the following format

```
    {
        "supplierName: "abc...",
        "supplierLink" : "www.abc.com" // null if not available,
        "imageUrl" : "...." // null if not available
    }
```