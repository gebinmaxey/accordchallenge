"""
A web scrapper utility to scrap the supplier details from the website
"""

import time
import requests
import sys
from bs4 import BeautifulSoup
import json


def make_request(base_url, page_counter):
    """
    Function to hit the specified URI with parameter(s) & fetch the HTML page components
    :param base_url:
    :param page_counter:
    :return html_soup:
    """
    url = base_url
    params = dict(
        page=page_counter
    )
    try:
        response = requests.get(url=url, params=params)
        html_soup = BeautifulSoup(response.text, 'html.parser')
    except requests.exceptions.RequestException as request_exception:
        print(request_exception)
        sys.exit(1)

    return html_soup


def make_json_result(supplier_name, supplier_link, supplier_logo):
    """
    Function to create the result JSON object
    :param supplier_name:
    :param supplier_link:
    :param supplier_logo:
    :return json_object:
    """
    json_object = {
        'supplierName': supplier_name,
        'supplierLink': supplier_link,
        'imageUrl': supplier_logo
    }

    return json_object


def parse_dom_components(html_soup):
    """
    Function to parse the HTML components & fetch the data
    :param html_soup:
    :return page_supplier_list:
    """
    page_supplier_list = []
    view_rows = html_soup.findAll('div', attrs={'class': 'views-row'})
    for row in view_rows:

        # Get logo details
        supplier_logo = None
        logo_div = row.find('div', attrs={'class': 'views-field-field-member-logo'})
        logo_img = logo_div.find('img', attrs={'class': 'img-responsive'})
        if logo_img:
            supplier_logo = logo_img['src']

        # Get supplier details
        supplier_link = None
        supplier_name = None
        title_div = row.find('div', attrs={'class': 'views-field-title'})
        title_span = title_div.find('span', attrs={'class': 'field-content'})
        title_link = title_span.find('a')
        if title_link:
            supplier_link = title_link['href']
            supplier_name = title_link.text
        else:
            supplier_name = title_span.text

        # Make JSON object
        json_result_obj = make_json_result(supplier_name, supplier_link, supplier_logo)
        page_supplier_list.append(json_result_obj)

    return page_supplier_list


def serialize_data(parse_result):
    """
    Function to persist the data into file
    :param parse_result:
    :return: None
    """
    with open('amfori_result_data.json', 'w') as outfile:
        json.dump(parse_result, outfile)


def validate_page_result(html_soup):
    """
    Function to check whether the page contains valid result
    :param html_soup:
    :return is_valid: boolean value
    """
    is_valid = False
    if html_soup.findAll('div', attrs={'class': 'views-row'}):
        is_valid = True

    return is_valid


def execute_operation():
    """
    Function to execute the set of function to scrap te supplier details
    :return: None
    """
    supplier_list_all = []
    base_url = "https://www.amfori.org/members"
    for page_counter in range(0, 102):
        print("Processing page {} ".format(page_counter))
        html_soup = make_request(base_url, page_counter)
        # Check the response contains data
        if not validate_page_result(html_soup):
            print("The data on the page index {} not found! Aborting ... ". format(page_counter))
            break
        page_supplier_list = parse_dom_components(html_soup)
        supplier_list_all.append(page_supplier_list)

    print(json.dumps(supplier_list_all))
    serialize_data(supplier_list_all)


if __name__ == '__main__':
    start_time = time.time()
    execute_operation()
    print("--- Task Executed in %s seconds ---" % (time.time() - start_time))
